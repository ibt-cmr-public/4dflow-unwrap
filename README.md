# 4dflow Unwrap
This repository contains all data associated to the ISMRM 2024 abstract: "4Dflow-unwrap: A collection of python-based phase unwrapping algorithms". 

## Phase Unwrapping in 4D Data

This repository contains a Python script designed to demonstrate the capabilities of various phase unwrapping algorithms on 2,3 and 4-dimensional data. The script provides implementations for several unwrapping methods and visualizes their effects on a sample dataset.

## Overview

Phase unwrapping is a crucial step in processing phase images obtained from phase constrast MRI, where each voxel's phase is represented as an angle within `[0, 2π)`. This script tackles the challenge of discontinuities in phase images, which can arise due to the inherent periodicity of angle measurements.

The codebase includes a set of functions designed to handle and unwrap 4D phase data using different algorithms. Each algorithm aims to correct these discontinuities by considering the spatial (and temporal, if applicable) coherence of phase values.

## Algorithms Demonstrated

- **Laplacian Unwrapping (`lap4D`,`lap3D`)**: Uses Laplacian-based methods to unwrap the phase.
- **Non-Continuous Path with Reliability Sorting (`nprs`)**: Employs a path-following scheme in the unwrapping process, incorporating a reliability sorting mechanism.
- **Graph Cut 3D Unwrapping (`gc3D`,`gc4D`)**: Applies a graph-based method tailored for 3D data to unwrap phase discontinuities using the principles of graph cuts.

## Additional functions
- **Global phase wraps (`total_field_correction`)**: Tries to correct global phase wraps by looking at temporal consistency.
- **Brute force (`brute_unwrap`)**: Tries to correct wrapped isolated voxels by iterative flipping of phase to minimize phase discontinuity. 

## Usage
'''python
#Example Usage                                               #
phi_w = [88, 96, 19, 21] # wrapped phase data [X,Y,Z,cardiac_phases] E [-pi,pi]            
phi_u3D=unwrap_data(phi_w, mode='3D') # 3D Laplacian unwrapping                 
phi_u4D=unwrap_data(phi_w, mode='4D', venc=0.5, ts=2) # 4D Laplacian unwrapping (with VENC mult.)  
'''

### Prerequisites
- numpy
- h5py
- scipy
- skimage
- tqdm
- maxflow (if graph unwrapping wants to be used)

Ensure these libraries are installed to run the script without issues.

## Example Output

The script [plot_gifs]('example/plot_gifs.ipynb') processes an HDF5 file containing 4D data (`Data-PI/Al/Al.hdf5` and its corresponding mask `Data-PI/Al/mask.hdf5`). It computes the magnitude and velocity from the phase data, normalizes these measurements, and applies various unwrapping algorithms.

Visualization of the unwrapped output is handled through GIF animations, which showcase the effectiveness of each algorithm. 

Let's create wrapped phase data (venc = 0.5m/s) from the original data: <br />
![](example/geometry_stack_venc_0.5.gif)

We can isolate the phase using an aortic mask: <br />
![](example/target_unwrapping_None_venc_0.5.gif)

Now we can use the algorithms presented in this work to unwrap the phase. For example we can use the 4D Laplacian approach: <br />
![](example/target_unwrapping_lap4D_venc_0.5.gif)

Or the nprs approach: <br />
![](example/target_unwrapping_nprs_venc_0.5.gif)

Or the 3Dgc approach: <br />
![](example/target_unwrapping_gc3D_venc_0.5.gif)

## Additional cases:

This example phase contrast MRI data was extracted from: https://codeocean.com/capsule/0115983/tree/v1

## Future Work

As part of the ongoing development of this repository, we plan to expand the range of unwrapping techniques offered, particularly by exploring advanced algorithms that incorporate flow regularizations. One such direction includes enforcing divergence-free fields in 3D, which can significantly enhance the accuracy and robustness of phase unwrapping in fluid dynamics and other applications involving vector fields.

Additionally, we aim to integrate machine learning-based approaches to phase unwrapping. An example of such a development is the implementation of [`PHU-NET`](https://pubmed.ncbi.nlm.nih.gov/34272757/) or similar deep learning models, which promise to improve unwrapping performance by learning optimal strategies from large datasets of wrapped and unwrapped phases.

We welcome contributions from the community to help implement these features and to suggest other potential improvements. If you have ideas or experience with phase unwrapping, flow regularization, or machine learning and are interested in contributing, please reach out or submit a pull request.

## Disclaimer
This code is under development and has not been fully tested for all potential use cases. It is provided "as is", without warranty of any kind, express or implied. Users should use it at their own risk. The developers of this code do not assume any responsibility for any harm or loss resulting from its use.

## License

This project is licensed under the MIT License - see the LICENSE file for details.

---

For more detailed information about the algorithms and their implementations, refer to the individual function docstrings within the script.